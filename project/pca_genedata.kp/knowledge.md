---
authors:
- ankit.kumar@elucidata.io
created_at: 2019-05-16 00:00:00
tags:
- PCA
thumbnail: images/output_10_1.png
title: PCA of Gene Dataset
tldr: Principal Component Analysis of Gene Dataset to check whether the dataset is
  good or not
updated_at: 2019-05-17 10:19:48.077666
---

We have one file that has gene data and other that has meta data. Gene data contains 32 columns, initial 2 being serial number and gene name respectively. Corresponding to each gene (a row) it has 30 different values. These values correspond to different samples corresponding to the column they are in. In meta data, the first column
corresponds to the sample names in the gene data. The next column is the Time column which corresponds to the time at which this sample was taken. There are 3 samples for each time point.

### Explaination of Code

Importing libraries


```python
import csv
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.decomposition import PCA
```
Reading the file. Here, I have resolved all the issues with the input file, if any.

1. I've assumed that missing values will give me an empty string, i.e, "". If there are any missing values: [out of S_(3j+i), i=1,2,3]
    <br>1. If only one of those three is missing, take average of other two and replace the missing value by it.
    <br>2. If two of three are missing, replace them by the non-missing value.
    <br>3. If all three are missing, discard the row.

2. If instead of a numerical value or a missing value, if say a string is present, I again discard that row.

3. If the input file doesn't have exactly 32 columns, the program stops by giving the corresponding error (indicating whether columns are more or less than 32)


```python
def readFile1(file1):
    file=open(file1,"r")
    reader=csv.reader(file)
    x=[]
    skip=False
    j=0
    for line in reader:
        fault=False
        if skip==False:
            try:
                a=line[31]
            except:
                print("Gene file cannot have less than 32 columns")
                return None
            try:
                a=line[32]
                print("Gene file cannot have more than 32 columns")
                return None
            except:
                skip=True
            continue
        try:
            for i in range(2,32):
                if fault==True:
                    break
                if line[i]=="":
                    if i%3==0:
                        line[i]=str((float(line[i-1])+float(line[i-2]))/2)
                    elif i%3==2:
                        if line[i+1]=="":
                            line[i]=line[i-1]
                            line[i+1]=line[i-1]
                        else:
                            line[i]=str((float(line[i-1])+float(line[i+1]))/2)
                    else:
                        if line[i+1]=="":
                            if line[i+2]=="":
                                fault=True
                            else:
                                line[i]=line[i+2]
                                line[i+1]=line[i+2]
                        elif line[i+2]=="":
                            line[i]=line[i+1]
                            line[i+2]=line[i+1]
                        else:
                            line[i]=str((float(line[i+1])+float(line[i+2]))/2)
                else:
                    float(line[i])                    
        except:
            fault=True
            j+=1
        if fault==False:
            temp=[]
            for i in range(2,32):
                temp.append(float(line[i]))
            x.append(temp)
    if j>1:
        print(str(j)+" lines ignored.")
    elif j==1:
        print("1 line ignored.")
    return x
```
This is my "main" function. It calls the readFile() function and if a None value is received in x, execution stops giving an appropriate error. I've used max norm to normalise the dataset before using PCA. Since a 2D plot is needed, 2 components will do. I've used different colours to indicate different timepoints at which the samples were taken. 


```python
def pca(file1,file2):
    #Reading data
    x=readFile1(file1)
    if x==None:
        return
    x=np.asarray(x)
    x=x.T
    
    #normalising data
    mean=np.mean(x,axis=0)
    for i in range(0,x[0].shape[0]):
        for j in range(0,x.shape[0]):
           x[j][i]-=mean[i]
    max=np.max(abs(x),0)
    for i in range(0,x[0].shape[0]):
        for j in range(0,x.shape[0]):
            x[j][i]/=max[i]

    pca=PCA(n_components=2)
    principalComponents=pca.fit_transform(x)
    principalDf=pd.DataFrame(data = principalComponents
                 , columns = ['principal component 1', 'principal component 2'])
    c_x=[]
    c_y=[]
    label=[]
    principalDf=np.asarray(principalDf)
    for j in range(0,10):
        for i in range(3):
            c_x.append(principalDf[3*j+i][0])
        for i in range(3):
            c_y.append(principalDf[3*j+i][1])
        for i in range(3):
            label.append(j)    
                
    colors = ['red','green','blue','pink','yellow','orange','black','grey','bisque','darkcyan']
    plt.scatter(c_x,c_y,c=label,cmap=matplotlib.colors.ListedColormap(colors))
    plt.xlabel('PC1')
    plt.ylabel('PC2')
    plt.savefig("plot.png")
```
It just calls the pca function, with appropriate arguments.


```python
pca("Assignment-DS_gene_data.csv","Assignment-DS_Meta_data_sheet_.csv")
```
    1 line ignored.




![png](images/output_10_1.png)


1 line ignored indicates that it discarded one row. On verification, one (and only one) row indeed had strings instead of values. 

The plot gives 10 clusters with 3 points each, and indicates that the dataset is good, because the samples taken at one particular timepoint must have close values.